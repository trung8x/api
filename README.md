# README #

### What is this repository for? ###

* Project is restful APIS that supports connect to smart contract and blockchain.

### Dev stack

* NodeJs 8.11.2
* restify framework: http://restify.com/
* restify-router: https://www.npmjs.com/package/restify-router
* bunyan log: https://github.com/trentm/node-bunyan
* web3.js 0.20: https://web3js.readthedocs.io
* truffle framework: http://truffleframework.com/

### How do I get set up? ###

* Install Docker: https://docs.docker.com/install
* Install Docker Compose: https://docs.docker.com/compose/install
* Go to source folder
* Run docker container: `docker-compose up`
* Container expose port 3000, plz should run api service on port 3000
* Stop/Start container: docker stop/start sharering
* Remove container: docker-compose down
* Remove docker image: docker rmi sharering

### How to run ###

* Attach to commandline in container: `docker exec -it sharering /bin/bash`
* Execute command in container: `docker exec sharering [bash command]`
 