require("babel-register");
require("babel-polyfill");

module.exports = {
	networks: {
		testrpc: {
			host: "localhost",
			port: 8545,
			network_id: "*",
			gas: 3000000,
			gasPrice: 30000000000
		},
		rinkeby: {
			host: "someIP",
			port: 8545,
			network_id: "*", // Match any network id
			gas: 3000000,
			gasPrice: 30000000000
		},
		mainnet: {
			host: "someIP",
			port: 8545,
			network_id: "*", // Match any network id
			gas: 3000000,
			gasPrice: 30000000000
		}
	}
};