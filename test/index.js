const restify = require('restify-clients');
const configs = require('../server/config').test;
const assert = require('assert');

// init the test client
let client = restify.createJsonClient({
	url: 'http://127.0.0.1:8080',
	version: '*'
});

describe('service: user endpoint', function () {
	// Test #1
	describe('signup check', function () {
		it('should get fail (existed email) with data.result = false', function (done) {
			const options = {
				path: configs.api_prefix + '/user/signup',
				headers: {
					'x-device': 'web',
					'x-language': 'en'
				}
			};
			client.post(options, {
				"email": "manh@gmail.com",
				"name": "David Teo",
				"password": "123456"
			}, function (err, req, res, data) {
				if (err) {
					throw new Error(err);
				} else {
					assert(!data.result, 'Signup duplicate email');
					done();
				}
			});
		});
	});

	// Test #2
	describe('login check with web device', function () {
		it('should get success with data.result format', function (done) {
			const options = {
				path: configs.api_prefix + '/user/login',
				headers: {
					'x-device': 'web',
					'x-language': 'en'
				}
			};
			client.post(options, {
				"email": "manh@gmail.com",
				"password": "123456"
			}, function (err, req, res, data) {
				if (err) {
					throw new Error(err);
				} else {
					assert(data.result, 'Login fail or wrong format!');
					done();
				}
			});
		});
	});

	// Test #3
	describe('login check with ios device', function () {
		it('should get success with data.success format', function (done) {
			const options = {
				path: configs.api_prefix + '/user/login',
				headers: {
					'x-device': 'ios',
					'x-language': 'en'
				}
			};
			client.post(options, {
				"email": "manh@gmail.com",
				"password": "123456"
			}, function (err, req, res, data) {
				if (err) {
					throw new Error(err);
				} else {
					assert(data.success, 'Login fail or wrong format!');
					done();
				}
			});
		});
	});

	// Test #4
	describe('acl check', function () {
		it('should get fail when search', function (done) {
			let headers = {
					'x-device': 'web',
					'x-language': 'en'
				},
				options = {
					path: configs.api_prefix + '/user/login',
					headers: headers
				};

			client.post(options, {
				"email": "manh@gmail.com",
				"password": "123456"
			}, function (err, req, res, data) {
				if (err) {
					throw new Error(err);
				} else {
					assert(data.result, 'Login fail or wrong format!');
					assert(data.data.uuid, 'Result not include uuid!');

					options.path = configs.api_prefix + '/user/search';
					headers.uuid = data.data.uuid;
					client.get(options, function (err, req, res, data) {
						if (!err) {
							throw new Error('User still access api search!');
						} else {
							assert(data.code === 'InvalidCredentials', 'Wrong error!');
							done();
						}
					});
				}
			});
		});
	});

	// Test #5
	describe('logout check', function () {
		it('should get fail login after logout', function (done) {
			let headers = {
					'x-device': 'web',
					'x-language': 'en'
				},
				options = {
					path: configs.api_prefix + '/user/login',
					headers: headers
				};

			client.post(options, {
				"email": "admin@gmail.com",
				"password": "123456"
			}, function (err, req, res, data) {
				if (err) {
					throw new Error(err);
				} else {
					assert(data.result, 'Login fail or wrong format!');
					assert(data.data.uuid, 'Result not include uuid!');

					options.path = configs.api_prefix + '/user/logout';
					headers.uuid = data.data.uuid;
					client.post(options, {}, function (err, req, res, data) {
						if (err) {
							throw new Error(err);
						} else {
							assert(data.result, data.error);

							options.path = configs.api_prefix + '/user/search';
							client.get(options, function (err, req, res, data) {
								if (!err) {
									throw new Error(err);
								} else {
									assert(data.code === 'InvalidCredentials', 'Wrong error!');
									done();
								}
							});
							done();
						}
					});
				}
			});
		});
	});
});