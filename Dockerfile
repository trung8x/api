FROM node:8
WORKDIR /var/workspace

# update the repository sources list
# and install dependencies
RUN apt-get update \
    && apt-get install -y curl git python build-essential \
    && apt-get -y autoclean

RUN npm install -g truffle
RUN npm install -g ganache-cli
RUN npm install -g pm2

COPY package.json .
RUN npm install

EXPOSE 3000
