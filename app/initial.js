var config = global.config;

var mysql = require('./db/functions/mysql');
var myWatcher = require('./watchers/somecontract.watcher');
var container = require('./controllers/container.controller');


var Initial = function () {}

Initial.initialize = function () {
    var self = this;
    this.initMySQL(function () {
        self.watchSomeContract();
    });
}

/**
 * Init MySQL
 */
Initial.initMySQL = function (callback) {
    mysql.init(function (er) {
        if (er) throw new Error(er);
        callback();
    });
}

Initial.watchSomeContract = function () {
    logger.info('Watch event at SomeContract address: ' + global.SomeContractAddress);
    var fromBlock;
    myWatcher.watchEvents(global.global.SomeContractAddress, fromBlock);
}

module.exports = Initial;