const Router = require('restify-router').Router;
const router = new  Router();

router.get('/hello/:name', function (req, res, next) {
	res.send('Hello ' + req.params.name);
	next();
});

module.exports = function(server) {
	router.applyRoutes(server);
};