module.exports = function (server) {
	require('./blockchain.routes')(server);
	require('./hello.routes')(server);
};
