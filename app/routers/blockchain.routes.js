const blockchainController = require('../controllers/blockchain.controller');
const Router = require('restify-router').Router;
const router = new  Router();

router.get('/', blockchainController.test);
router.get('', blockchainController.test);

module.exports = function(server) {
	router.applyRoutes(server, '/test');
};