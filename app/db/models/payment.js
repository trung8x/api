var DataTypes = require('sequelize');


var PaymentHistory = global.db_connection.define('PaymentHistory', {
    buyer: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    tokenAddress: {
        type: DataTypes.STRING
    },
    paymentId: {
        type: DataTypes.STRING,
        primaryKey: true
    },
    value: {
        type: DataTypes.STRING
    },
    block: {
        type: DataTypes.STRING
    },
    tx: {
        type: DataTypes.STRING,
        primaryKey: true
    }
});

module.exports = PaymentHistory;
