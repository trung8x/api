var Payment = require('./../models/payment');

var CREATION_ERROR = new Error('Table creation error.');

var DB = function () {}

DB.init = function (callback) {
    Payment.sync({
        force: false
    }).then((re) => {
        if (!re) return callback(CREATION_ERROR);
    }).catch((ex) => {
        return callback(ex);
    });
}

module.exports = DB;