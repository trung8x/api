var config = require('./configs/config');

// Smart contracts list
var SomeContract = artifacts.require('./SomeContract.sol');
var SomeContractABI = require('./../build/contracts/SomeContract.json').abi;
var SomeContractContract = web3.eth.contract(SomeContractABI);
var SomeContractAddress = config.smartcontract.SomeContractAddress;

// Accounts list
var accounts = web3.eth.accounts;
var OwnerAddress = accounts[config.smartcontract.OwnerIndex];


var Global = function () {}

Global.globalize = function (name, data) {

    if (name && data) {
        global[name] = data;
        return;
    }

    // Globalize smart contracts
    global.SomeContract = SomeContract;
    global.SomeContractABI = SomeContractABI;
    global.SomeContractContract = SomeContractContract;
    global.SomeContractAddress = SomeContractAddress;

    // Globalize accounts
    global.OwnerAddress = OwnerAddress;
}

module.exports = Global;