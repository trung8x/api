var mysql = require('./../db/functions/mysql');
var utils = require('./../libs/helpers/utils');
var Events = require('./../libs/classes/events');

const DUPLICATION_ERROR = 'Duplicate event.';


var Handlers = function () {}

Handlers.handleEventTransfer = function (event) {
    logger.trace('my.watcher::handleEventTransfer');
}

var SomeContractWatcher = {

    watchEvents: function (address, fromBlock) {
        // If load all tokens when restart, increase of latest block by 1
        if (fromBlock !== 'latest' && typeof fromBlock === 'string') {
            fromBlock = parseInt(fromBlock) + 1;
        }

        // If not, watch contract which is created
        var contractInstance = global.SomeContract.at(address)
        var listener = contractInstance.allEvents({
            fromBlock: fromBlock
        });

        var events = new Events(listener, null, null);
        events.addEvent('Transfer', Handlers.handleEventTransfer);
        events.watch();
    }
}

module.exports = SomeContractWatcher;