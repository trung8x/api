var config = global.config;
var utils = require('./../libs/helpers/utils');
var rate = require('./../libs/helpers/rate');

const RATE_ERROR = 'Cannot update new rate.';
const UNLOCK_ERROR = 'Cannot unlock account.';
const GASPRICE_ERROR = 'Cannot get gasPrice.';
const NONCE_ERROR = 'Cannot get nonce.';


var Container = {

    updateRate: function () {
        logger.trace('updateRate');

        rate.getRate(function (_rate) {
            if (!_rate) {
                return logger.error('updateRate::getRate::error: ' + RATE_ERROR);
            }

            logger.trace('updateRate::getRate::success: ' + _rate);

            utils.unlock(global.OwnerAddress, config.smartcontract.passphrase, function (re) {
                if (!re) {
                    return logger.error('updateRate::unlock::error: ' + UNLOCK_ERROR);
                }

                utils.getGasPrice(function (gasPrice) {
                    if (!gasPrice) {
                        return logger.error('updateRate::getGasPrice::error: ' + GASPRICE_ERROR);
                    }

                    var nonce = utils.getNonce(global.OwnerAddress, 'latest');
                    if (!nonce && nonce != 0) {
                        return logger.error('updateRate::getNonce::error: ' + NONCE_ERROR);
                    }

                    global.RateContract.at(global.RateContractAddress).setRate(_rate, {
                        from: global.OwnerAddress,
                        gas: 50000,
                        gasPrice: gasPrice,
                        nonce: nonce
                    }, function (er, tx) {
                        if (er) {
                            return logger.error('updateRate::setRate::error: ' + er);
                        }
                        return logger.trace('updateRate::txid: ' + tx);
                    });
                });
            });
        });
    },

    getRateFromSmartContract: function (req, res) {
        logger.trace('getRateFromSmartContract');

        global.RateContract.at(global.SomeContractAddress).ETH_USD_rate({
            from: global.OwnerAddress
        }, function (er, re) {
            if (er) {
                logger.error('ETH_USD_rate::error: ' + er);
                return res.send({
                    state: 'error',
                    error: global.properties.get('error_501')
                });
            }

            logger.trace('ETH_USD_rate::success: ' + re);
            return res.send({
                state: 'success',
                data: {
                    unit: 'CENT/ETH',
                    value: re
                }
            });
        });
    }
};

module.exports = Container;