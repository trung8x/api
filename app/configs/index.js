global.env = process.env.NODE_ENV || 'development';

module.exports = {
	server: require('./common')[global.env],
	database: require('./database')[global.env],
	blockchain: require('./smartcontract')[global.env],
	partner: require('./partners')[global.env]
};
