const errors = require("restify-errors");

module.exports = {
	checkHeaderParams: function (req, res, next) {
		next();
	},
	isUserAuthenticated: function(req, res, next) {
		next(new errors.InvalidCredentialsError('Unauthorized!'));
	}
};

