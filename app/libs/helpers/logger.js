const bunyan = require('bunyan');

/**
* Constructor
*/
const Logger = function () {
    this.bunyan = bunyan.createLogger({
        src: (global.env === 'development'),
        name: 'sharering',
        streams: [
            {
                level: 'debug',
                stream: process.stdout
            },
            {
                type: 'rotating-file',
                level: 'info',
                period: '7d',
                path: __dirname + '/../../logs/log.json'
            }
        ],
        serializers: bunyan.stdSerializers,
        level: 'debug',
        env: global.env
    });

    /**
     * Trace
     */
    this.trace = function (content) {
        this.bunyan.trace(content);
    };

    /**
     * Debug
     */
    this.debug = function (content, ...args) {
        this.bunyan.debug(content, args);
    };

    /**
     * Info
     */
    this.info = function (content, ...args) {
        this.bunyan.info(content, args);
    };

    /**
     * Warning
     */
    this.warn = function (content) {
        this.bunyan.warn(content);
    };

    /**
     * Error
     */
    this.error = function (content) {
        this.bunyan.error(content);
    };

    /**
     * Fatal
     */
    this.fatal = function (content) {
        this.bunyan.fatal(content);
    };
};

module.exports = new Logger();