const request = require('request');
const BigNumber = require('bignumber.js');
const web3 = require("web3");

const ERROR = 'Error';

const Utils = function () {};

Utils.unlock = function (address, password, callback) {
    if (global.env === 'development') return callback(true);
    web3.personal.unlockAccount(address, password, function (er, re) {
        if (!er) return callback(true);
        return callback(false);
    });
};

Utils.isJSON = function (s) {
    try {
        JSON.parse(s);
        return true;
    } catch (e) {
        return false;
    }
};

Utils.parseJSON = function (s) {
    try {
        s = JSON.parse(s);
        return s;
    } catch (e) {
        return null;
    }
};

Utils.isAddress = function (address) {
    try {
        return web3.isAddress(address);
    } catch (er) {
        return false;
    }
};

Utils.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

Utils.sortArray = function (array, key, decrease) {
    return array.sort(function (a, b) {
        if (decrease) return b[key] - a[key];
        return a[key] - b[key];
    });
};

Utils.httpGet = function (router, callback) {
    let self = this;
    request.get(router, (er, res, body) => {
        if (er) return callback(er, null);
        if (!self.isJSON(body)) return callback(ERROR, null);
        body = JSON.parse(body);
        return callback(null, body);
    });
};

Utils.httpPost = function (router, params, callback) {
    let self = this;
    request.post(router, {
        form: params
    }, (er, res, body) => {
        if (er) return callback(er, null);
        if (!self.isJSON(body)) return callback(ERROR, null);
        body = JSON.parse(body);
        return callback(null, body);
    });
};

Utils.getGasPrice = function (callback) {
    const GAS_STATION_URL = 'https://ethgasstation.info/json/predictTable.json';
    const CARED_KEY = 'expectedWait';
    const RETURNED_KEY = 'gasprice';
    const UNDER = 2; // blocks;

    Utils.httpGet(GAS_STATION_URL, function (er, re) {
        if (er) {
            try {
                return callback(web3.eth.gasPrice.toString(10));
            } catch (er) {
                return callback(null);
            }
        }
        Utils.sortArray(re, CARED_KEY, true);
        while (re.length > 0 && re[0][CARED_KEY] > UNDER) re.shift();
        if (re.length == 0) return callback(null);
        return callback((re[0][RETURNED_KEY] * (10 ** 9)).toString());
    });
};

Utils.parseRawInt = function (i) {
    try {
        var r = new BigNumber(i);
        return r.toString(10);
    } catch (er) {
        return null;
    }
};

Utils.getNonce = function (address, status) {
    try {
        var actualStatus = status || 'latest';
        return web3.eth.getTransactionCount(address, actualStatus);
    } catch (er) {
        return null;
    }
};

Utils.getBlockNumber = function () {
    try {
        return web3.eth.blockNumber;
    } catch (er) {
        return null;
    }
};

Utils.formatQuery = function(queryObject) {
	let querys = [];
	for (let key in queryObject) {
		// check if the property/key is defined in the object itself, not in parent
		if (queryObject.hasOwnProperty(key) && ('' + queryObject[key]).length > 0) {
			querys.push(key + '=' + queryObject[key]);
		}
	}
	return querys.length > 0 ? '?' + querys.join('&') : '';
};

Utils.isFunction = function(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
};

module.exports = Utils;