class Events {
    static WARNING = 'Event or Handler is empty.';

    constructor(listener, nameOfEvent, handlers) {
        this.listener = listener;
        if (!nameOfEvent || typeof nameOfEvent != 'string') this.nameOfEvent = [];
        else this.nameOfEvent = [nameOfEvent];
        if (!handlers || typeof handlers != 'function') this.handlers = [];
        else this.handlers = [handlers];
    }

    watch() {
        // Make sure dont listen on many tunnels
        this.listener.stopWatching();

        var self = this;
        this.listener.watch(function (er, re) {
            if (er) throw new Error(er);
            if (self.nameOfEvent.length == 0 || self.handlers.length == 0) return logger.warn(WARNING);

            var length = self.nameOfEvent.length;
            for (var i = 0; i < length; i++) {
                if (re.event == self.nameOfEvent[i]) {
                    return self.handlers[i](re);
                }
            }
        });
    }

    addEvent(nameOfEvent, handler) {
        if (!nameOfEvent || typeof nameOfEvent != 'string') return logger.warn(WARNING);
        else this.nameOfEvent.push(nameOfEvent);

        if (!handler || typeof handler != 'function') return logger.warn(WARNING);
        else this.handlers.push(handler);
    }
}

module.exports = Events;