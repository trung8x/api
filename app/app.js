const restify = require('restify');
// var sequelize = require('sequelize');
// var cors = require('cors');
// var bodyParser = require('body-parser');
const propertiesReader = require('properties-reader');

// const assertJump = require("./../test/helpers/assertJump");

// const logger = require('./libs/helpers/logger');

// const _ = config.mysql;
// const db_connection = new sequelize(`mysql://${_.username}:${_.password}@${_.host}/${_.dbName}`, {
//     logging: false
// });

// global.artifacts = artifacts;
// global.web3 = web3;
// global.db_connection = db_connection;
global.configs = require('./configs') || {server: {}};
global.logger = require('./libs/helpers/logger');
global.properties = propertiesReader(__dirname + '/properties.file');
// require('./global').globalize();

let server = restify.createServer({log: global.logger.bunyan});

// Extend logger using the plugin.
server.use(restify.plugins.requestLogger());
server.use(restify.plugins.queryParser({
	mapParams: true
}));
server.use(restify.plugins.bodyParser({
	mapParams: true
}));

// check header params before every request
server.pre(require('./middlewares').checkHeaderParams);

// config router
require('./routers')(server);

//
// require('./initial').initialize();
//

// require by truffle
module.exports = function (deployer) {
	deployer();
	// truffle finish initilize
	server.listen(global.configs.server.port, function () {
		global.logger.info('%s listening at %s', server.name, server.url);

	});
};